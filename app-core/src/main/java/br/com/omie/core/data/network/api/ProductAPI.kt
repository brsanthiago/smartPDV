package br.com.omie.core.data.network.api

import br.com.omie.core.data.model.ProductResponse
import retrofit2.http.GET

interface ProductAPI {

    @GET("products?limit=10&skip=10")
    suspend fun getProducts(): ProductResponse
}