package br.com.omie.core.data.dto

import android.os.Parcelable
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.model.Product
import kotlinx.parcelize.Parcelize

@Parcelize
data class CartItemDTO(
    val id: Long? = null,
    val cart: Cart? = null,
    val cartId: Long? = null,
    val product: Product? = null,
    var quantity: Int? = null
) : Parcelable
