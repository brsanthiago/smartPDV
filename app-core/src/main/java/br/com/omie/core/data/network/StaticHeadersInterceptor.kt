package br.com.omie.core.data.network

import br.com.omie.core.di.SmartPDVKoinComponent
import okhttp3.Interceptor
import okhttp3.Response

internal class StaticHeadersInterceptor(
    private val staticHeaders: Map<String, String>
) : Interceptor, SmartPDVKoinComponent {

    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        val builder = request()
            .newBuilder()

        staticHeaders.forEach { (header, value) ->
            builder.addHeader(header, value)
        }

        val response = proceed(builder.build())

        return response
    }

}