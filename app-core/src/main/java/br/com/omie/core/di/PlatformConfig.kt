package br.com.omie.core.di

import android.content.Context
import br.com.omie.core.AppInjectable
import br.com.omie.core.AppModuleConfig
import br.com.omie.core.data.network.response.AppVoid

class PlatformConfig(
    internal val requiresAppHash: Boolean,
    val controller: PlatformController
) : AppModuleConfig {

    /**
     * Contains permissions requested at platform launch.
     * Should not contain flow-specific permissions, like camera.
     */
    private val requiredPermissions = mutableSetOf<String>()

    /**
     *  Adds a permission in platform requirements.
     *  @param permission the permission that should be requested at platform launch.
     */
    fun addRequiredPermission(permission: String) {
        requiredPermissions.add(permission)
    }

    fun getPermissions() = requiredPermissions.toSet()

}

interface PlatformController : AppInjectable {

    suspend fun dispatchReset(context: Context)

    fun startSplash(context: Context) {
        // Default implementation
    }

    /*
        By implementing this method, platform has the chance to execute anything necessary before
        or after each network call.
        By default, returns original network call without any modification.
        Designed to be used within Rx flows.
     */
    fun <T> adaptNetworkCall(source: T): T {
        return source
    }

    /*
        Similar to above, but with Coroutines
     */
    suspend fun adaptNetworkCallAsync(): AppVoid {
        return AppVoid.INSTANCE
    }

    /*
        Similar to above, but with Coroutines
     */
    suspend fun handleInvalidExternalTokenAsync(status: String): AppVoid {
        return AppVoid.INSTANCE
    }

}