package br.com.omie.core.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "_cart_item_")
data class CartItem(
    @PrimaryKey(true)
    val id: Long? = null,
    val cartId: Long? = null,
    val productId: Long? = null,
    var quantity: Int,
) : Parcelable
