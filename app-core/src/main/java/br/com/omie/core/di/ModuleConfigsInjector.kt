package br.com.omie.core.di

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build

class ModuleConfigsInjector {

    fun inject(context: Context) {
        val appInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.packageManager.getApplicationInfo(
                context.packageName,
                PackageManager.ApplicationInfoFlags.of(PackageManager.GET_META_DATA.toLong())
            )
        } else {
            @Suppress("DEPRECATION")
            context.packageManager.getApplicationInfo(
                context.packageName,
                PackageManager.GET_META_DATA
            )
        }

        val bundle = appInfo.metaData
        val injectors = mutableListOf<ModuleConfigInjector>()

        bundle.keySet()
            .filter { it.contains(METADATA) }
            .map {
                val value = bundle.getString(it) ?: return@map

                try {
                    val loadedClass = Class.forName(value)
                    if (ModuleConfigInjector::class.java.isAssignableFrom(loadedClass)) {
                        val injector = loadedClass.getDeclaredConstructor().newInstance()
                                as ModuleConfigInjector

                        injectors.add(injector)
                    } else {
                        //TODO add log
                    }
                } catch (e: Exception) {
                    //TODO add log
                }
            }

        injectors.sortedBy { it.getOrder() }.forEach {
            it.inject(context)
            //TODO add log
        }
    }

    companion object {
        private const val METADATA = "br.com.omie.smartpdv.di.APP_MODULE_CONFIG_INJECTOR"
    }

}