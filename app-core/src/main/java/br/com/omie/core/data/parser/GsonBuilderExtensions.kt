package br.com.omie.core.data.parser

import com.google.gson.Gson
import com.google.gson.GsonBuilder

fun GsonBuilder.createCustomGson(): Gson =
    GsonBuilder()
        .registerTypeAdapterFactory(EnumTypeAdapterFactory())
        .create()