package br.com.omie.core.extensions

import br.com.omie.core.data.parser.createCustomGson
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

inline fun <reified T> Gson.fromJsonWithAutoTypecast(json: String): T =
    this.fromJson(json, object : TypeToken<T>() {}.type)

/*
    Gson may not have enough information to auto typecast when working with lists
    In that case, use method below
 */
inline fun <reified T> parseList(json: String, typeToken: Type): T {
    return GsonBuilder().createCustomGson().fromJson(json, typeToken)
}