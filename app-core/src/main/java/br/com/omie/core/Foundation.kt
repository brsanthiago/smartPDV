package br.com.omie.core

import android.content.Context
import br.com.omie.core.data.database.SmartDatabase
import br.com.omie.core.di.ModuleConfigsInjector
import br.com.omie.core.di.SmartPDVKoinComponent
import br.com.omie.core.di.SmartPDVKoinContext

object Foundation : SmartPDVKoinComponent {

    lateinit var params: FoundationParams
        private set

    val moduleConfigs = mutableListOf<AppModuleConfig>()

    fun init(context: Context, init: FoundationParams.() -> Unit) {
        val params = FoundationParams()
        params.init()

        this.params = params.apply {
            build(context)
        }
        SmartDatabase.invoke(context)

        SmartPDVKoinContext.setup(context)
        // Configs must be injected after koin setup
        ModuleConfigsInjector().inject(context)
    }

    inline fun <reified T : AppModuleConfig> getModuleConfig(): T? {
        val config = moduleConfigs.firstOrNull { it is T }
        return config as? T
    }

    inline fun <reified T : AppModuleConfig> injectModuleConfig(moduleConfig: T) {
        if (getModuleConfig<T>() != null) {
            throw IllegalStateException("ModuleConfig ${T::class.java.name} already injected. Cannot override it.")
        }
        moduleConfigs.add(moduleConfig)
    }

    class FoundationParams {
        var baseUrl = ""
        internal fun build(context: Context) {
            baseUrl = context.getString(R.string.baseUrl)
        }
    }

}

interface AppModuleConfig

interface AppInjectable
