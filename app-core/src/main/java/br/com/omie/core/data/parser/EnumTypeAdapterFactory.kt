package br.com.omie.core.data.parser


import br.com.omie.core.data.network.response.AppVoid
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException
import java.util.*

/*
    Type adapter factory used exclusively by enums.
    If value being deserialized is not founded,
    it tries to avoid return null by returning UNKNOWN in case enum type have such constant.
 */
internal class EnumTypeAdapterFactory : TypeAdapterFactory {
    override fun <T> create(gson: Gson?, type: TypeToken<T>?): TypeAdapter<T>? {
        if (gson == null || type == null) {
            return null
        }

        val rawType = type.rawType as Class<T>

        return when {
            rawType == AppVoid::class.java -> {
                VoidAdapter()
            }

            rawType.isEnum -> {
                EnumTypeAdapter(rawType)
            }

            else -> {
                null
            }
        }
    }

    private class EnumTypeAdapter<TT> constructor(classOfT: Class<TT>) : TypeAdapter<TT>() {
        private val nameToConstant: MutableMap<String, TT?> = HashMap()
        private val constantToName: MutableMap<TT, String> = HashMap()

        init {
            val enumConstants = classOfT.enumConstants as Array<out Enum<*>>

            for (constant in enumConstants) {
                var name = constant.name
                val serializedName: SerializedName? = try {
                    classOfT.getField(name).getAnnotation(SerializedName::class.java)
                } catch (e: NoSuchFieldException) {
                    null
                }
                if (serializedName == null) {
                    name = constant.toString()
                } else {
                    name = serializedName.value
                    for (alternate in serializedName.alternate) {
                        nameToConstant[alternate] = constant as TT
                    }
                }
                nameToConstant[name] = constant as TT
                constantToName[constant] = name
            }
        }

        @Throws(IOException::class)
        override fun read(`in`: JsonReader): TT? {
            if (`in`.peek() == JsonToken.NULL) {
                `in`.nextNull()
                return null
            }
            val token = `in`.nextString()
            val value = nameToConstant[token]
            return value
                ?: if (nameToConstant.containsKey("unknown")) {
                    nameToConstant["unknown"]
                } else if (nameToConstant.containsKey("UNKNOWN")) {
                    nameToConstant["UNKNOWN"]
                } else {
                    null
                }
        }

        @Throws(IOException::class)
        override fun write(out: JsonWriter, value: TT?) {
            out.value(if (value == null) null else constantToName[value])
        }
    }

    private class VoidAdapter<TT> : TypeAdapter<TT>() {
        @Throws(IOException::class)
        override fun read(`in`: JsonReader): TT? {
            `in`.skipValue()
            return null
        }

        @Throws(IOException::class)
        override fun write(out: JsonWriter, value: TT?) {
            val v: String? = null
            out.value(v)
        }
    }
}