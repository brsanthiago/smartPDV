package br.com.omie.core.di

import android.content.ComponentCallbacks
import androidx.activity.ComponentActivity
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.CreationExtras
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.core.parameter.ParametersHolder
import org.koin.core.qualifier.Qualifier

/**
 * A note about following extensions.
 *
 * Any object that extends SmartPDVKoinComponent will use our custom SmartPDVKoinContext, regardless which
 * injection method will be used (KoinComponent.inject, ComponentActivity.viewModel, etc).
 *
 * However, would be easy to fall into error of use above injection methods in classes
 * that does not implement SmartPDVKoinContext, thus mix Koin contexts (the custom one for classes
 * that implement SmartPDVKoinContext and the global/default one in the contrary case).
 *
 * Therefore, extensions below serves as a shield against that scenario, given that extend
 * SmartPDVKoinContext is a must to appInject, appViewmodel, etc be available.
 */


/**
 * Injects a view model of type T into an activity that also is a ArcaKoinViewModelOwner.
 */
@MainThread
inline fun <reified T : ViewModel, A> A.appViewmodel(
    qualifier: Qualifier? = null,
    noinline extrasProducer: (() -> CreationExtras)? = null,
    noinline parameters: (() -> ParametersHolder)? = null,
): Lazy<T> where A : SmartPDVViewModelOwner, A : ComponentActivity {
    return lazy(LazyThreadSafetyMode.NONE) {
        getViewModel(qualifier, extrasProducer, parameters)
    }
}

/**
 * Injects a view model of type T into an fragment that also is a SmartPDVViewModelOwner.
 */
@MainThread
inline fun <reified T : ViewModel, F> F.appViewmodel(
    qualifier: Qualifier? = null,
    noinline ownerProducer: () -> ViewModelStoreOwner = { this },
    noinline extrasProducer: (() -> CreationExtras)? = null,
    noinline parameters: (() -> ParametersHolder)? = null,
): Lazy<T> where F : Fragment, F : SmartPDVViewModelOwner {
    return lazy(LazyThreadSafetyMode.NONE) {
        getViewModel(qualifier, ownerProducer, extrasProducer, parameters)
    }
}

interface SmartPDVViewModelOwner : SmartPDVKoinComponent, ComponentCallbacks