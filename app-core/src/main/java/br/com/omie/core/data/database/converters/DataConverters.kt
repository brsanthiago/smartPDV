package br.com.omie.core.data.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.Date

class DataConverters {
    @TypeConverter
    fun fromList(value: List<String>): String {

        return Gson().toJson(value)
    }

    @TypeConverter
    fun toList(value: String): List<String> {
        return try {
            val listType: Type = object : TypeToken<List<String?>?>() {}.type
            GsonBuilder().create().fromJson(value, listType)
        } catch (e: Exception) {
            emptyList()
        }
    }

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}