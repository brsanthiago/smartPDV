package br.com.omie.core.di

class SmartPDVConfig() : SmartPDVModuleConfig {

    /**
     * Contains permissions requested at platform launch.
     * Should not contain flow-specific permissions, like camera.
     */
    private val requiredPermissions = mutableSetOf<String>()

    /**
     *  Adds a permission in platform requirements.
     *  @param permission the permission that should be requested at platform launch.
     */
    fun addRequiredPermission(permission: String) {
        requiredPermissions.add(permission)
    }

    fun getPermissions() = requiredPermissions.toSet()

}


interface SmartPDVModuleConfig

interface SmartPDVInjectable