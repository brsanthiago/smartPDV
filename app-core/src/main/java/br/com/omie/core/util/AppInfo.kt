package br.com.omie.core.util

import android.util.Log
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Is imperative that classes on this file be obfuscated.
 */
interface AppClientInfoProtocol {

    /**
     * Level of OkHttp logs
     */
    val networkLoggerLevel: HttpLoggingInterceptor.Level

    /**
     * Base url of mobile services host
     */
    val baseUrl: String
}

class AppInfo(
    override val baseUrl: String,
) : AppClientInfoProtocol {

    init {
        AppRawInfo.buildFrom(this)
        AppRawInfo.debug()

        AppRawIssuerInfo.buildFrom(this)
        AppRawIssuerInfo.debug()
    }

    override val networkLoggerLevel: HttpLoggingInterceptor.Level =
        HttpLoggingInterceptor.Level.NONE
}

/*
    Should contain only data used by Foundation. Should not be public.
 */
internal object AppRawInfo {

    private val initialized = AtomicBoolean(false)
    var baseUrl: String = ""; private set

    fun buildFrom(clientInfo: AppClientInfoProtocol) {
        if (initialized.get()) {
            return
        }

        initialized.set(true)

        baseUrl = clientInfo.baseUrl
    }

    fun debug() {
        Log.i("AppCenterLogger", "AppRawInfo \n baseUrl: $baseUrl")
    }
}

/*
    Should contain only related data related with issuer.
 */
object AppRawIssuerInfo {

    private val initialized = AtomicBoolean(false)


    fun debug() {
        Log.i("AppLogger", "AppRawInfo \n")
    }

    fun buildFrom(clientInfo: AppClientInfoProtocol) {
        if (initialized.get()) {
            return
        }

        initialized.set(true)

    }
}