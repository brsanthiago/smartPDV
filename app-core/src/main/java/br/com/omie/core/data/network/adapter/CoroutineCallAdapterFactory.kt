package br.com.omie.core.data.network.adapter

import br.com.omie.core.Foundation
import br.com.omie.core.data.network.exception.AppHttpError
import br.com.omie.core.data.network.exception.AppHttpErrorType
import br.com.omie.core.data.network.exception.AppHttpException
import br.com.omie.core.data.network.response.AppVoid
import br.com.omie.core.data.parser.createCustomGson
import br.com.omie.core.di.PlatformConfig
import br.com.omie.core.di.PlatformController
import br.com.omie.core.extensions.fromJsonWithAutoTypecast
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import okhttp3.Request
import okio.Timeout
import retrofit2.*
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicInteger

internal class CoroutineCallAdapterFactory(
    private val shouldProcessFailures: Boolean = false
) : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ) = when (getRawType(returnType)) {
        Call::class.java -> {
            val successType = getParameterUpperBound(0, returnType as ParameterizedType)
            val allowsEmptyResponse = getRawType(successType) == AppVoid::class.java
            BodyAdapter(
                type = successType,
                allowsEmptyResponse = allowsEmptyResponse,
                shouldProcessFailures = shouldProcessFailures
            )
        }

        else -> null
    }

}

internal class BodyAdapter(
    private val type: Type,
    private val allowsEmptyResponse: Boolean,
    private val shouldProcessFailures: Boolean
) : CallAdapter<Type, Call<Type>> {

    override fun responseType() = type

    override fun adapt(call: Call<Type>): Call<Type> {
        return BodyCall(
            call = call,
            retriesCounter = AtomicInteger(),
            allowsEmptyResponse = allowsEmptyResponse,
            shouldProcessFailures = shouldProcessFailures
        )
    }
}

internal class BodyCall<T>(
    call: Call<T>,
    private val retriesCounter: AtomicInteger,
    private val allowsEmptyResponse: Boolean,
    private val shouldProcessFailures: Boolean
) :
    CallDelegate<T, T>(call), NetworkAdapterComponent {

    private val scope = CoroutineScope(Job() + Dispatchers.IO)

    private val platformController: PlatformController? by lazy {
        Foundation.getModuleConfig<PlatformConfig>()?.controller
    }

    override fun cancel() {
        super.cancel()
        scope.cancel()
    }

    @Suppress("USELESS_CAST")
    override fun enqueueImpl(callback: Callback<T>) {
        platformController?.apply {
            scope.launch {
                runCatching {
                    adaptNetworkCallAsync()
                }.fold(
                    onSuccess = {
                        enqueueThroughDelegate(callback)
                    },
                    onFailure = {
                        val url = (call.request() as? Request)?.url.toString()
                        handleFailureResponse(
                            callback = callback,
                            exception = AppHttpException.fromThrowable(
                                throwable = it,
                                url = url
                            )
                        )
                    }
                )
            }
        } ?: run {
            enqueueThroughDelegate(callback)
        }
    }

    override fun cloneImpl() = BodyCall(
        call = call.clone(),
        retriesCounter = retriesCounter,
        allowsEmptyResponse = allowsEmptyResponse,
        shouldProcessFailures = shouldProcessFailures
    )

    override fun timeout(): Timeout {
        return call.timeout()
    }

    @Suppress("USELESS_CAST")
    private fun enqueueThroughDelegate(callback: Callback<T>) {
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                val url = (call.request() as? Request)?.url.toString()

                if (response.isSuccessful) {
                    handleSuccessfulResponse(
                        callback = callback,
                        response = response,
                        url = url
                    )
                } else {
                    handleFailureResponse(
                        callback = callback,
                        response = response,
                        url = url
                    )
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                val url = (call.request() as? Request)?.url.toString()
                val exception = AppHttpException.fromThrowable(
                    throwable = t,
                    url = url
                )

                handleFailureResponse(
                    callback = callback,
                    exception = exception
                )
            }
        })
    }

    private fun handleSuccessfulResponse(
        callback: Callback<T>,
        response: Response<T>,
        url: String?
    ) {
        val body = response.body()

        when {
            allowsEmptyResponse -> {
                callback.onResponse(this, Response.success(AppVoid.INSTANCE as T))
            }

            body == null && !allowsEmptyResponse -> {
                val exception = AppHttpException(
                    type = AppHttpErrorType.UNKNOWN,
                    throwable = IllegalArgumentException("Response is mandatory, but received null body"),
                    url = url
                )

                handleFailureResponse(
                    callback = callback,
                    exception = exception
                )
            }

            body != null -> {
                callback.onResponse(this, Response.success(body))
            }
        }
    }

    private fun handleFailureResponse(
        callback: Callback<T>,
        response: Response<T>,
        url: String?
    ) {
        val error = response.errorBody()?.string().orEmpty()
        val code = response.code()

        val customError = try {
            GsonBuilder().createCustomGson().fromJsonWithAutoTypecast<AppHttpError>(error)
        } catch (e: Exception) {
            null
        }

        val exception = AppHttpException(
            code = code,
            type = AppHttpErrorType.fromHttpErrorCode(code),
            httpError = customError,
            throwable = null,
            url = url
        )

        handleFailureResponse(callback, exception)
    }

    /*
        Scenario 1
            Original exception:
                Unable to load log servers with log-list.json failed to load with
                java.net.ConnectException: Failed to connect to www.gstatic.com
            Final exception:
                javax.net.ssl.SSLPeerUnverifiedException: Certificate transparency failed
            Treatment:
                Retries the request, with limit of MAX_RETRIES

        Scenario 2
            Exception:
                Http exception with code 412 (invalid external token)
            Treatment:
                Calls security provider's refresh token method then retries the request
            How to simulate:
                At SessionHeaderCrypto, use empty vat.
    */
    private fun handleFailureResponse(
        callback: Callback<T>,
        exception: AppHttpException
    ) {

        val failureDispatcher = {
            val failure = if (shouldProcessFailures) process(exception) else exception
            callback.onFailure(this, failure)
        }

        val retryDispatcher = {
            clone().enqueue(callback)
        }


        failureDispatcher.invoke()
    }

}

internal abstract class CallDelegate<TIn, TOut>(
    protected val call: Call<TIn>
) : Call<TOut> {
    override fun execute(): Response<TOut> = throw NotImplementedError()
    final override fun enqueue(callback: Callback<TOut>) = enqueueImpl(callback)
    final override fun clone(): Call<TOut> = cloneImpl()

    override fun cancel() = call.cancel()
    override fun request(): Request = call.request()
    override fun isExecuted() = call.isExecuted
    override fun isCanceled() = call.isCanceled

    abstract fun enqueueImpl(callback: Callback<TOut>)
    abstract fun cloneImpl(): Call<TOut>
}