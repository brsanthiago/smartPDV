package br.com.omie.core.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.omie.core.data.model.Product

@Dao
interface ProductDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(items: List<Product>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveOne(item: Product)

    @Query("SELECT * FROM _product_ ORDER BY title, id DESC")
    suspend fun findAll(): List<Product>

    @Query("SELECT * FROM _product_ WHERE id = :id")
    fun findOne(id: Long): Product?

    @Query("SELECT COUNT(id) FROM _product_")
    suspend fun getCount(): Int
}