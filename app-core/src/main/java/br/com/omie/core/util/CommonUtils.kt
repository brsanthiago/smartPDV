package br.com.omie.core.util

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

object CommonUtils {
    fun formatCurrencyValue(value: Double): String {
        val symbols = DecimalFormatSymbols(Locale.getDefault())
        symbols.groupingSeparator = '.'
        symbols.decimalSeparator = ','
        val format = DecimalFormat("###,##0.00", symbols)
        format.roundingMode = RoundingMode.HALF_EVEN
        return "R$ ${format.format(value)}"
    }
}