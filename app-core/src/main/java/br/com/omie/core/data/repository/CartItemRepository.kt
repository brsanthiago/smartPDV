package br.com.omie.core.data.repository

import br.com.omie.core.data.database.SmartDatabase
import br.com.omie.core.data.model.CartItem

class CartItemRepository(database: SmartDatabase) {
    private val dao = database.cartItemDao()

    suspend fun findAll(cartId: Long) = dao.findAll(cartId)

    fun saveOne(cart: CartItem) = dao.saveOne(cart)

    fun findOne(id: Long) = dao.findOne(id)

    suspend fun findByCart(cartId: Long, productId: Long) = dao.findByCart(cartId, productId)

    fun getCount() = dao.getCount()

    suspend fun deleteItem(id: Long) = dao.deleteItem(id)

    suspend fun deleteByCard(cartId: Long) = dao.deleteByCard(cartId)

}