package br.com.omie.core.data.repository

import br.com.omie.core.data.database.SmartDatabase
import br.com.omie.core.data.model.Cart

class CartRepository(database: SmartDatabase) {
    private val dao = database.cartDao()

    suspend fun actives() = dao.actives()

    suspend fun findAll() = dao.findAll()

    fun saveOne(cart: Cart) = dao.saveOne(cart)

    fun findOne(id: Int) = dao.findOne(id)

    suspend fun delete(id: Long) = dao.delete(id)

    fun getCount() = dao.getCount()

}