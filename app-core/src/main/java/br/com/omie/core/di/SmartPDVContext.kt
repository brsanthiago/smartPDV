package br.com.omie.core.di

import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.Koin
import org.koin.core.KoinApplication
import org.koin.core.component.KoinComponent
import org.koin.core.module.Module
import org.koin.dsl.koinApplication


object SmartPDVKoinContext {
    var koinApplication: KoinApplication? = null; private set

    fun setup(context: Context) {
        if (koinApplication != null) {
            return
        }

        koinApplication = koinApplication {
            androidContext(context)
        }
    }

    fun loadModules(modules: List<Module>) {
        koinApplication?.modules(modules)
    }

    fun loadModules(module: Module) {
        koinApplication?.modules(module)
    }

}

interface SmartPDVKoinComponent : KoinComponent {

    override fun getKoin(): Koin {
        return SmartPDVKoinContext.koinApplication?.koin
            ?: throw IllegalStateException("SmartPDVKoinComponent: koin not initialized")
    }

}