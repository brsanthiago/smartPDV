package br.com.omie.core.data.network.response

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

sealed class AppResult<out T> {
    data class Success<out T>(val value: T?) : AppResult<T>()
    data class Failure(val error: Exception?) : AppResult<Nothing>()
    data object Loading : AppResult<Nothing>()
    data object Iddle : AppResult<Nothing>()

    companion object {

        fun <T> from(result: Result<T>): AppResult<T> {
            return result.fold(
                onSuccess = { data ->
                    Success(data)
                },
                onFailure = { failure ->

                    Failure(failure as? Exception)
                }
            )
        }

        /**
         * Maps an [kotlin.Result](result) of type T to an [ AppResult] of type R.
         * @param result the result that should be transformed.
         * @param transform the transformation function.
         */
        fun <T, R> map(result: Result<T>, transform: (T) -> R): AppResult<R> {
            return result.fold(
                onSuccess = { data ->
                    Success(transform(data))
                },
                onFailure = { failure ->

                    Failure(failure as? Exception)
                }
            )
        }

    }

}

inline fun <S> AppResult<S>.fold(
    onSuccess: ((S?) -> Unit),
    onFailure: ((Exception?) -> Unit),
    noinline onLoading: ((() -> Unit)?),
    noinline onIddle: (() -> Unit)? = null
) {
    when (this) {
        is AppResult.Success -> onSuccess(value)
        is AppResult.Failure -> onFailure(error)
        is AppResult.Loading -> onLoading?.invoke()
        is AppResult.Iddle -> onIddle?.invoke()
    }
}

fun <S> AppResult<S>?.isSuccess() = this is AppResult.Success

fun <S> AppResult<S>?.isLoading() = this is AppResult.Loading

fun <S> AppResult<S>?.isFailure() = this is AppResult.Failure

fun <S> AppResult<S>?.error() = (this as? AppResult.Failure)?.error

fun <S> AppResult<S>?.data() = (this as? AppResult.Success)?.value

fun <T> Flow<T>.asResult(): Flow<AppResult<T>> {
    return this
        .map<T, AppResult<T>> {
            AppResult.Success(it)
        }
        .onStart {
            emit(AppResult.Loading)
        }
        .catch {
            emit(AppResult.Failure(it as? Exception))
        }
}
