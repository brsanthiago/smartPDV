package br.com.omie.core.data.network

import br.com.omie.core.data.network.adapter.CoroutineCallAdapterFactory
import br.com.omie.core.data.parser.createCustomGson
import br.com.omie.core.di.SmartPDVKoinComponent
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

internal interface AppRetrofitBuilder {
    fun getCallAdapterFactory(): CallAdapter.Factory
}

/**
 * Class responsible to build okhttp and retrofit
 */
private class AppRetrofit constructor(
    private val serviceConnector: AppServiceConnector,
    private val logLevel: HttpLoggingInterceptor.Level,
    private val callAdapterFactory: CallAdapter.Factory
) : SmartPDVKoinComponent {

    fun <T> buildApi(apiClass: Class<T>): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(serviceConnector.getBaseUrl())
            .client(buildOkHttpClient())
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().createCustomGson()))
            .build()

        return retrofit.create(apiClass)
    }

    private fun buildOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .followRedirects(true)

        val logging = HttpLoggingInterceptor()
        logging.level = logLevel
        httpClient.addInterceptor(logging)


        return httpClient.build()
    }

    companion object {
        private const val REQUEST_TIMEOUT = 60
    }
}

/**
 * Base class for [AppRetrofit] builders
 */
internal abstract class AppBaseRetrofitBuilder(protected val serviceConnector: AppServiceConnector) :
    AppRetrofitBuilder {

    private var logLevel: HttpLoggingInterceptor.Level = HttpLoggingInterceptor.Level.NONE

    fun <T> build(apiClass: Class<T>): T =
        AppRetrofit(
            serviceConnector = serviceConnector,
            logLevel = logLevel,
            callAdapterFactory = getCallAdapterFactory()
        )
            .buildApi(apiClass)
}

/**
 * An [AppRetrofit] builder that adapts calls to be compatible with coroutines
 */
internal class AppCoroutinesRetrofitBuilder(serviceConnector: AppServiceConnector) :
    AppBaseRetrofitBuilder(serviceConnector) {

    override fun getCallAdapterFactory(): CallAdapter.Factory {
        return CoroutineCallAdapterFactory(
            shouldProcessFailures = serviceConnector.shouldProcessFailures()
        )
    }

}
