package br.com.omie.core.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.omie.core.data.model.Cart

@Dao
interface CartDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveOne(item: Cart)

    @Query("SELECT * FROM _cart_ WHERE active = 1 ORDER BY id ASC")
    suspend fun actives(): List<Cart>

    @Query("SELECT * FROM _cart_ WHERE active = 0 ORDER BY id ASC")
    suspend fun findAll(): List<Cart>

    @Query("SELECT * FROM _cart_ WHERE id = :id")
    fun findOne(id: Int): Cart?

    @Query("SELECT COUNT(id) FROM _cart_ WHERE active = 1")
    fun getCount(): Int

    @Query("DELETE FROM _cart_ WHERE id = :id")
    suspend fun delete(id: Long): Int
}