package br.com.omie.core.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.util.Date

@Parcelize
@Entity(tableName = Cart.CART_TABLE)
data class Cart(
    @PrimaryKey(true)
    val id: Long? = null,
    val customer: String,
    var paymentDate: Date? = null,
    var totalItems: Int? = null,
    var totalAmount: Double? = null,
    var active: Boolean
) : Parcelable {
    companion object {
        const val CART_TABLE = "_cart_"
    }
}