package br.com.omie.core.data.repository

import br.com.omie.core.data.database.SmartDatabase
import br.com.omie.core.data.model.Product

class ProductRepository(database: SmartDatabase) {
    private val dao = database.productDao()

    suspend fun findAll() = dao.findAll()

    fun saveAll(products: List<Product>) = dao.saveAll(products)

    fun saveOne(product: Product) = dao.saveOne(product)

    fun findOne(id: Long) = dao.findOne(id)

    suspend fun getCount() = dao.getCount()

}