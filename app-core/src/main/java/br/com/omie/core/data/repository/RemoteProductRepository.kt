package br.com.omie.core.data.repository

import br.com.omie.core.data.network.api.ProductAPI

class RemoteProductRepository(private val api: ProductAPI) {

    suspend fun getProducts() = api.getProducts()

}