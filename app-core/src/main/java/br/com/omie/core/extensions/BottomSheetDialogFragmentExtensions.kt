package br.com.omie.core.extensions

import android.app.Dialog
import android.view.View
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

fun BottomSheetDialogFragment.expand() {
    val observer = object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            view?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
            val dialog = dialog as? BottomSheetDialog ?: return
            val bottomSheet =
                dialog.findOptional<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                    ?: return
            val behavior = BottomSheetBehavior.from<View>(bottomSheet)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = 0
            behavior.isHideable = false

            BottomSheetBehavior.from(bottomSheet).apply {
                setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onSlide(bottomSheet: View, slideOffset: Float) {
                        // Do nothing
                    }

                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                            state = BottomSheetBehavior.STATE_HIDDEN
                        }
                    }
                })
            }

        }
    }

    view?.viewTreeObserver?.addOnGlobalLayoutListener(observer)
}

fun BottomSheetDialogFragment.disableDrag(dialog: Dialog) {
    dialog.setOnShowListener {
        val bottomSheet = dialog
            .findOptional<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
            ?: return@setOnShowListener

        val behavior = BottomSheetBehavior.from<View>(bottomSheet)

        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // Do nothing
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        })
    }
}