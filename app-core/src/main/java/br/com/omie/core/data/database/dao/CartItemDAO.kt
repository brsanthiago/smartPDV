package br.com.omie.core.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.omie.core.data.model.CartItem

@Dao
interface CartItemDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveOne(item: CartItem)

    @Query("SELECT * FROM _cart_item_ WHERE cartId = :cartId ORDER BY id ASC")
    suspend fun findAll(cartId: Long): List<CartItem>

    @Query("SELECT * FROM _cart_item_ WHERE id = :id")
    fun findOne(id: Long): CartItem?

    @Query("SELECT * FROM _cart_item_ WHERE cartId = :cartId AND productId = :productId")
    suspend fun findByCart(cartId: Long, productId: Long): CartItem?

    @Query("SELECT COUNT(id) FROM _cart_item_")
    fun getCount(): Int

    @Query("DELETE FROM _cart_item_ WHERE id = :itemId")
    suspend fun deleteItem(itemId: Long): Int

    @Query("DELETE FROM _cart_item_ WHERE cartId = :cartId")
    suspend fun deleteByCard(cartId: Long): Int
}