package br.com.omie.core.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import br.com.omie.core.data.database.converters.DataConverters
import br.com.omie.core.data.database.dao.CartDAO
import br.com.omie.core.data.database.dao.CartItemDAO
import br.com.omie.core.data.database.dao.ProductDAO
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.model.CartItem
import br.com.omie.core.data.model.Product

@Database(
    entities = [Product::class, Cart::class, CartItem::class], version = 1, exportSchema = false
)
@TypeConverters(DataConverters::class)
abstract class SmartDatabase : RoomDatabase() {
    companion object {
        private const val SECRET_DATA_APP_NAME = "smart_pdv_data"

        @Volatile
        private var instance: SmartDatabase? = null
        private val LOCK = Any()

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                SmartDatabase::class.java,
                SECRET_DATA_APP_NAME
            ).fallbackToDestructiveMigration()
                .build()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }
    }

    abstract fun productDao(): ProductDAO
    abstract fun cartDao(): CartDAO
    abstract fun cartItemDao(): CartItemDAO
}