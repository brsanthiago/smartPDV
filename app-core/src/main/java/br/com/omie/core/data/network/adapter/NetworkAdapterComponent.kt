package br.com.omie.core.data.network.adapter

import br.com.omie.core.data.network.exception.AppHttpException
import br.com.omie.core.di.SmartPDVKoinComponent
import java.util.concurrent.atomic.AtomicInteger

private const val MAX_RETRIES = 3

internal interface NetworkAdapterComponent : SmartPDVKoinComponent {

    fun process(exception: AppHttpException): AppHttpException {
        return exception
    }

    fun isRetriesExhausted(retriesCounter: AtomicInteger) =
        retriesCounter.incrementAndGet() > MAX_RETRIES

}