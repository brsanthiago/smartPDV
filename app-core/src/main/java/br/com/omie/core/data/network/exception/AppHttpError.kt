package br.com.omie.core.data.network.exception

import android.os.Parcelable
import br.com.omie.core.data.parser.createCustomGson
import br.com.omie.core.extensions.fromJsonWithAutoTypecast
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import retrofit2.HttpException
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.SSLHandshakeException
import javax.net.ssl.SSLPeerUnverifiedException

internal fun Throwable.isSSLException() =
    this is SSLPeerUnverifiedException ||
            (this is AppHttpException
                    && this.throwable != null
                    && this.throwable is SSLPeerUnverifiedException)

internal fun Throwable.isInvalidExternalTokenException(): Boolean {
    return when (this) {
        is HttpException -> {
            AppHttpErrorType.fromHttpErrorCode(code()) ==
                    AppHttpErrorType.INVALID_EXTERNAL_TOKEN
        }

        is AppHttpException -> {
            type == AppHttpErrorType.INVALID_EXTERNAL_TOKEN
        }

        else -> false
    }
}

@Parcelize
data class AppHttpException(
    val code: Int = 0,
    val type: AppHttpErrorType,
    val httpError: AppHttpError? = null,
    val throwable: Throwable?,
    var handledAutomatically: Boolean = false,
    val url: String? = null
) : Exception(throwable), Parcelable {

    fun toJson(maxSizeInBytes: Int): String {
        val fullJson = Gson().toJson(this)
        val size = fullJson.toByteArray().size

        return if (size > maxSizeInBytes) {
            val simplifiedException = AppHttpException(
                code = this.code,
                type = this.type,
                httpError = this.httpError,
                handledAutomatically = this.handledAutomatically,
                throwable = null,
                url = url
            )
            Gson().toJson(simplifiedException)
        } else {
            fullJson
        }
    }

    companion object {
        const val EXTRA = "AppHttpExceptionExtra"

        fun fromThrowable(
            throwable: Throwable,
            url: String? = null
        ): AppHttpException {
            return when (throwable) {
                is AppHttpException -> {
                    throwable
                }

                is HttpException -> {
                    val errorBody = throwable.response()?.errorBody()?.string().orEmpty()

                    val serverResponse: AppHttpError? = try {
                        GsonBuilder().createCustomGson()
                            .fromJson(errorBody, AppHttpError::class.java)
                    } catch (error: Exception) {
                        null
                    }

                    AppHttpException(
                        code = throwable.code(),
                        type = AppHttpErrorType.fromHttpErrorCode(throwable.code()),
                        httpError = serverResponse,
                        throwable = throwable,
                        url = url
                    )
                }

                is SocketTimeoutException, is UnknownHostException, is ConnectException -> {
                    AppHttpException(
                        type = AppHttpErrorType.CONNECTION_ERROR,
                        throwable = throwable,
                        url = url
                    )
                }

                is SSLPeerUnverifiedException, is SSLHandshakeException -> {
                    AppHttpException(
                        type = AppHttpErrorType.OFFLINE_SERVER,
                        throwable = throwable.cause ?: throwable,
                        url = url
                    )
                }

                else -> {
                    AppHttpException(
                        type = AppHttpErrorType.UNKNOWN,
                        throwable = throwable,
                        url = url
                    )
                }
            }
        }

        fun fromResponse(response: Response<*>): AppHttpException {
            val error = response.errorBody()?.string().orEmpty()
            val code = response.code()

            val customError = try {
                GsonBuilder().createCustomGson().fromJsonWithAutoTypecast<AppHttpError>(error)
            } catch (e: Exception) {
                null
            }

            return AppHttpException(
                code = code,
                type = AppHttpErrorType.fromHttpErrorCode(code),
                httpError = customError,
                throwable = null
            )
        }

        fun getErrorType(throwable: Throwable): AppHttpErrorType {
            return when (throwable) {
                is AppHttpException -> throwable.type
                else -> AppHttpErrorType.UNKNOWN
            }
        }

        /*
            Only to facilitate test forbidden scenario
         */
        fun buildForbiddenException() =
            AppHttpException(
                code = 403,
                type = AppHttpErrorType.UNKNOWN,
                throwable = IllegalStateException("Forbidden"),
                handledAutomatically = false,
                httpError = AppHttpError(
                    status = "error",
                    error = "forbidden",
                    title = "Forbidden",
                    message = "Forbidden reason",
                    alertUI = AppHttpErrorAlert(
                        errorTitle = "Forbidden",
                        errorMessage = "Forbidden reason",
                        dismissTitle = "OK",
                        showSupportButton = true
                    )
                )
            )

        /*
            Only for debug purposes
        */
        fun buildInternalServerError(internalServerErrorCode: Int?) =
            AppHttpException(
                code = 400,
                type = AppHttpErrorType.SERVER_ERROR,
                throwable = IllegalStateException("Internal Server Error"),
                httpError = AppHttpError(
                    status = "internalServerError",
                    error = "internalServerError",
                    title = "Internal Server Error",
                    message = "Reason",
                    internalServerErrorCode = internalServerErrorCode,
                    alertUI = AppHttpErrorAlert(
                        errorTitle = "Internal Server Error $internalServerErrorCode",
                        errorMessage = "Reason",
                        dismissTitle = "OK",
                        showSupportButton = true
                    )
                )
            )
    }
}

@Parcelize
data class AppHttpError(
    val status: String? = null,
    val error: String? = null,
    @SerializedName("code") internal val internalServerErrorCode: Int? = null,
    @SerializedName("user-title-alert") var title: String? = null,
    @SerializedName("user-text-alert") val message: String? = null,
    @SerializedName("alert-ui") val alertUI: AppHttpErrorAlert?
) : Parcelable {

    fun isPendingTransaction() = error == "pending transaction"

    fun getInternalServerError() = AppInternalServerError.fromCode(internalServerErrorCode)
}

@Parcelize
data class AppHttpErrorAlert(
    @SerializedName("error-title") val errorTitle: String?,
    @SerializedName("error-message") val errorMessage: String?,
    @SerializedName("dismiss-title") val dismissTitle: String?,
    @SerializedName("show-support-button") val showSupportButton: Boolean = false,
    @SerializedName("action-url") val actionUrl: String? = null
) : Parcelable

@Parcelize
enum class AppHttpErrorType : Parcelable {
    CONNECTION_ERROR,
    NO_CONTENT,
    SERVER_ERROR,
    SERVER_ERROR_UNAUTHORIZED,
    UNAVAILABLE_FOR_LEGAL_REASONS,
    OFFLINE_SERVER,
    OUTDATED_APP,
    INVALID_EXTERNAL_TOKEN,
    INVALID_PLAY_SERVICES,
    INVALID_PASSWORD,
    UNKNOWN;

    companion object {
        fun fromHttpErrorCode(code: Int) = when (code) {
            401 -> SERVER_ERROR_UNAUTHORIZED
            404 -> NO_CONTENT
            412 -> INVALID_EXTERNAL_TOKEN
            423 -> OFFLINE_SERVER
            426 -> OUTDATED_APP
            451 -> UNAVAILABLE_FOR_LEGAL_REASONS
            else -> SERVER_ERROR
        }
    }
}

// Internal server error
@Parcelize
enum class AppInternalServerError(val code: Int) : Parcelable {
    // For instance, when SMS code is misinformed
    INVALID_CODE(15008),

    // Means that user or account is blocked
    SESSION_BLOCKED(15003),

    // For instance, when SMS code is misinformed many times
    TRIES_REACHED(15017),

    // User is trying to update email/phone with current one
    CURRENT_EMAIL_OR_PHONE(15019),

    // For instance, when user tries to pay bank slip on weekend
    PAYMENT_AFTER_TIME_WINDOW(6120),

    // When registered address on my cards features with same type address
    ADDRESS_TYPE_HAS_EXISTS(15031),

    // Indicates failure while obtaining access tokens (defined on mobile only).
    BASIC_AUTH_FAILURE(-999),

    UNKNOWN(-1);

    internal companion object {
        fun fromCode(code: Int?): AppInternalServerError {
            return values().firstOrNull { it.code == code } ?: UNKNOWN
        }
    }
}