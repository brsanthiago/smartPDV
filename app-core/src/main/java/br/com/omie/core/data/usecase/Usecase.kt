package br.com.omie.core.data.usecase

import br.com.omie.core.data.network.response.AppResult
import kotlinx.coroutines.flow.Flow

abstract class UseCase<in Params, out Type : Any?> {
    abstract suspend operator fun invoke(params: Params): AppResult<Type>
}

abstract class FlowableUseCase<in Params, out Type : Any> {
    abstract suspend operator fun invoke(params: Params): Flow<AppResult<Type>>
}