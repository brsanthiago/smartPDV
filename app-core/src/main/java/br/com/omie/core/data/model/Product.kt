package br.com.omie.core.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = Product.TABLE_NAME)
data class Product(
    @PrimaryKey
    val id: Long? = null,
    val title: String? = null,
    val description: String? = null,
    val thumbnail: String? = null,
    val images: List<String>? = null,
    val price: Double? = null,
) : Parcelable {
    companion object {
        const val TABLE_NAME = "_product_"
    }
}


