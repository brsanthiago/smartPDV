package br.com.omie.core.di

import android.content.Context

interface ModuleConfigInjector {
    fun getOrder(): Int = 0
    fun inject(context: Context)
}