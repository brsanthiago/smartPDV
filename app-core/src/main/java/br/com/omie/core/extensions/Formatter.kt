package br.com.omie.core.extensions

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

private val LOCALE_PT_BR = Locale("pt", "BR")

fun Date.toDateTime(): String =
    SimpleDateFormat("dd/MM/yyyy HH:mm", LOCALE_PT_BR).format(this)
