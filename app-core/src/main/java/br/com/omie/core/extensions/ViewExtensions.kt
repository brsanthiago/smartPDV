package br.com.omie.core.extensions

import android.app.Activity
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import br.com.omie.core.R


inline fun <reified T : View> View.findOptional(@IdRes id: Int): T? = findViewById(id) as? T

inline fun <reified T : View> Activity.findOptional(@IdRes id: Int): T? = findViewById(id) as? T

inline fun <reified T : View> Dialog.findOptional(@IdRes id: Int): T? = findViewById(id) as? T


fun FragmentActivity.showConfirmationDialog(
    title: String? = null,
    message: String? = null,
    primaryText: String? = null,
    secondaryText: String? = null,
    primaryActionButton: (() -> Unit)? = null,
    secondaryActionButton: (() -> Unit)? = null
): AlertDialog {

    val builder = AlertDialog.Builder(this)

    val inflater = layoutInflater

    val dialogLayout = inflater.inflate(R.layout.confirmation_dialog, null)

    val tvTitle = dialogLayout.findViewById<TextView>(R.id.tvTitle)
    val tvMessage = dialogLayout.findViewById<TextView>(R.id.tvMessage)
    val btnClear = dialogLayout.findViewById<Button>(R.id.btnClear)
    val btnClose = dialogLayout.findViewById<ImageButton>(R.id.btnClose)

    primaryText?.let {
        btnClear.text = it
    }

    message?.let {
        tvMessage.text = it
    }

    title?.let {
        tvTitle.text = it
    }

    builder.setView(dialogLayout)

    val dialog = builder.create()

    dialog.window?.setBackgroundDrawable(ColorDrawable(getColor(R.color.transparent)))


    btnClose.setOnClickListener {
        dialog.dismiss()
    }

    btnClear.setOnClickListener {
        primaryActionButton?.invoke()
        dialog.dismiss()
    }

    return dialog
}