package br.com.omie.core.data.network

import br.com.omie.core.Foundation
import br.com.omie.core.di.SmartPDVKoinComponent

/**
 * Contract to be implement by service builders
 */
internal interface AppServiceBuilder {
    fun <T> build(apiClass: Class<T>): T

    fun <T> internalBuild(
        apiClass: Class<T>,
        builder: AppBaseRetrofitBuilder,
        serviceConnector: AppServiceConnector
    ): T {
        return builder.build(apiClass)
    }
}


/**
 * Service builder for APIs compatible with coroutines
 */
class AppCoroutinesServiceBuilder(
    private val serviceConnector: AppServiceConnector = AppServiceConnector.MobileServices
) : AppServiceBuilder {

    override fun <T> build(apiClass: Class<T>): T {
        return internalBuild(
            apiClass = apiClass,
            builder = AppCoroutinesRetrofitBuilder(serviceConnector),
            serviceConnector = serviceConnector
        )
    }

}

/**
 * List of available connectors to remote services
 */
sealed class AppServiceConnector : ServiceConnectorContract {

    /**
     * Connector to mobile services
     */
    object MobileServices : AppServiceConnector(), SmartPDVKoinComponent {

        override fun getBaseUrl(): String {
            return Foundation.params.baseUrl
        }
    }

}

/**
 * Contract to be implemented by service connectors
 */
internal interface ServiceConnectorContract {

    fun getBaseUrl(): String

    fun shouldProcessFailures(): Boolean {
        return true
    }

}