package br.com.omie.core.data.network.response

class AppVoid private constructor() {
    companion object {
        val INSTANCE = AppVoid()
    }
}
