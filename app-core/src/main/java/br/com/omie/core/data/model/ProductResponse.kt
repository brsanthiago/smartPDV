package br.com.omie.core.data.model

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("products") val data: List<Product>
)
