package br.com.omie.smartpdv.ui.cart.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.network.response.data
import br.com.omie.smartpdv.ui.cart.usecase.AddCartUseCase
import br.com.omie.smartpdv.ui.cart.usecase.CountCartsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class NewCartViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val addCartUseCase: AddCartUseCase,
    private val countCartsUseCase: CountCartsUseCase
) : ViewModel() {
    private val _cartFlow = MutableStateFlow<AppResult<Unit>>(AppResult.Loading)
    val cartFlow: StateFlow<AppResult<Unit>> = _cartFlow


    var totalCarts = 0

    init {
        viewModelScope.launch(dispatcher) {
            totalCarts = countCartsUseCase(CountCartsUseCase.Params()).data() ?: 0
        }
    }

    fun addNewCart(customer: String) {
        _cartFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            val cart = Cart(customer = customer, active = true)

            addCartUseCase(AddCartUseCase.Params(cart)).onEach { result ->
                _cartFlow.value = result
            }.launchIn(this)
        }
    }
}