package br.com.omie.smartpdv.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.smartpdv.databinding.HistoryFragmentBinding
import br.com.omie.smartpdv.ui.history.adapter.HistoryAdapter
import br.com.omie.smartpdv.ui.history.viewmodel.HistoryViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

internal class HistoryFragment : Fragment(), SmartPDVViewModelOwner {
    private val viewModel: HistoryViewModel by appViewmodel()
    private var binding: HistoryFragmentBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return HistoryFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
        }.root
    }

    private fun configUI(carts: List<Cart>) {
        binding?.apply {
            val adapter = HistoryAdapter(carts, object : HistoryAdapter.Delegate {
                override fun onItemSelected(cart: Cart) {
                    findNavController().navigate(
                        HistoryFragmentDirections.actionHistoryFragmentToHistoryDetailFragment(
                            cart
                        )
                    )
                }

            })
            rvHistory.adapter = adapter
        }
    }

    private fun observeViewModel() {

        lifecycleScope.launch {

            viewModel.historyFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = { data ->
                            configUI(data.orEmpty())
                        },
                        onFailure = { error ->
                            println(error?.message)
                        }
                    )
                }
        }
    }
}