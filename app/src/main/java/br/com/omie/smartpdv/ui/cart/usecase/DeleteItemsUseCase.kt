package br.com.omie.smartpdv.ui.cart.usecase

import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartItemRepository
import br.com.omie.core.data.usecase.UseCase

internal class DeleteItemsUseCase(private val repository: CartItemRepository) :
    UseCase<DeleteItemsUseCase.Params, Int>() {
    class Params(val cartId: Long)

    override suspend fun invoke(params: Params): AppResult<Int> {
        return AppResult.from(Result.success(repository.deleteByCard(cartId = params.cartId)))
    }
}