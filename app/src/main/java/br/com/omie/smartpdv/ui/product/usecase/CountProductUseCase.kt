package br.com.omie.smartpdv.ui.product.usecase

import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.ProductRepository
import br.com.omie.core.data.usecase.FlowableUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class CountProductUseCase(private val repository: ProductRepository) :
    FlowableUseCase<CountProductUseCase.Params, Int>() {
    class Params

    override suspend fun invoke(params: Params): Flow<AppResult<Int>> = flow {
        emit(AppResult.Loading)

        val result = AppResult.from(
            runCatching {
                repository.getCount()
            }
        )
        emit(result)
    }
}