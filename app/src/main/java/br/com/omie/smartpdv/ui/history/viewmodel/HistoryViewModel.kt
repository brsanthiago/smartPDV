package br.com.omie.smartpdv.ui.history.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.smartpdv.ui.cart.usecase.HistoryUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class HistoryViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val getHistoryUseCase: HistoryUseCase
) : ViewModel() {


    private val _historyFlow = MutableStateFlow<AppResult<List<Cart>>>(AppResult.Loading)
    val historyFlow: StateFlow<AppResult<List<Cart>>> = _historyFlow

    init {
        getAll()
    }

    private fun getAll() {
        _historyFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            viewModelScope.launch(dispatcher) {
                getHistoryUseCase(HistoryUseCase.Params()).onEach { result ->
                    _historyFlow.value = result
                }.launchIn(this)
            }
        }
    }
}