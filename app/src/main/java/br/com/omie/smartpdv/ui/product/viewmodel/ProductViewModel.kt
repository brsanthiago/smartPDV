package br.com.omie.smartpdv.ui.product.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.model.Product
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.smartpdv.ui.cart.usecase.GetCartsUseCase
import br.com.omie.smartpdv.ui.product.usecase.ProductsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class ProductViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val getProductsUseCase: ProductsUseCase,
    private val getCartsUseCase: GetCartsUseCase,
) : ViewModel() {

    private val _productsFlow = MutableStateFlow<AppResult<List<Product>>>(AppResult.Loading)
    val productsFlow: StateFlow<AppResult<List<Product>>> = _productsFlow

    private val _cartsFlow = MutableStateFlow<AppResult<List<Cart>>>(AppResult.Loading)
    val cartsFlow: StateFlow<AppResult<List<Cart>>> = _cartsFlow

    var showCart = false

    fun init() {
        getProducts()
        getCarts()
    }

    private fun getProducts() {
        _productsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            getProductsUseCase(ProductsUseCase.Params()).onEach {
                _productsFlow.value = it
            }.launchIn(this)
        }
    }

    private fun getCarts() {
        _productsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            getCartsUseCase(GetCartsUseCase.Params()).onEach {
                _cartsFlow.value = it
            }.launchIn(this)
        }

    }
}