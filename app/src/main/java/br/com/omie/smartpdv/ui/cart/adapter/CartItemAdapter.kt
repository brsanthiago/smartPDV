package br.com.omie.smartpdv.ui.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.omie.core.data.dto.CartItemDTO
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.CartItemBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

internal class CartItemAdapter(
    private val items: List<CartItemDTO>,
    private val delegate: Delegate
) : RecyclerView.Adapter<CartItemAdapter.CartViewHolder>() {

    interface Delegate {
        fun onAddItem(item: CartItemDTO)
        fun onRemoveItem(item: CartItemDTO)
        fun onDeleteItem(item: CartItemDTO)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        return CartViewHolder(
            CartItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ).apply {

            with(binding) {
                btnAdd.setOnClickListener {
                    delegate.onAddItem(items[bindingAdapterPosition])
                }
                btnRemove.setOnClickListener {
                    delegate.onRemoveItem(items[bindingAdapterPosition])
                }
                btnDelete.setOnClickListener {
                    delegate.onDeleteItem(items[bindingAdapterPosition])
                }
            }
        }
    }

    override fun onBindViewHolder(vh: CartViewHolder, position: Int) {
        val cart = items[position]
        vh.bind(cart)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    internal class CartViewHolder(val binding: CartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context
        fun bind(item: CartItemDTO) {
            with(binding) {

                val quantity = item.quantity ?: 0

                if (quantity > 1) {
                    btnRemove.visibility = View.VISIBLE
                    btnDelete.visibility = View.GONE
                } else {
                    btnRemove.visibility = View.GONE
                    btnDelete.visibility = View.VISIBLE
                }

                tvQtde.text = item.quantity.toString()
                tvProduct.text = item.product?.title
                tvDescription.text = item.product?.description
                tvPrice.text = CommonUtils.formatCurrencyValue(item.product?.price ?: 0.0)

                item.product?.thumbnail?.let { image ->
                    try {
                        Glide.with(context)
                            .load(image)
                            .placeholder(R.drawable.product_placeholder)
                            .error(R.drawable.product_placeholder)
                            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                            .into(ivProduct)
                    } catch (_: Exception) {
                    }
                }

            }
        }
    }
}