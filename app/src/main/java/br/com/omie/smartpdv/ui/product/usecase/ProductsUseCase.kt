package br.com.omie.smartpdv.ui.product.usecase

import br.com.omie.core.data.model.Product
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.ProductRepository
import br.com.omie.core.data.usecase.FlowableUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class ProductsUseCase(private val repository: ProductRepository) :
    FlowableUseCase<ProductsUseCase.Params, List<Product>>() {
    class Params

    override suspend fun invoke(params: Params): Flow<AppResult<List<Product>>> = flow {
        emit(AppResult.Loading)

        val result = AppResult.from(
            runCatching {
                repository.findAll()
            }
        )
        emit(result)
    }
}