package br.com.omie.smartpdv.ui.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.model.Product
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.appViewmodel
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.FragmentProductsBinding
import br.com.omie.smartpdv.ui.cart.adapter.CartAdapter
import br.com.omie.smartpdv.ui.product.adapter.ProductAdapter
import br.com.omie.smartpdv.ui.product.viewmodel.ProductViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

internal class ProductsFragment : Fragment(), ProductAdapter.Delegate,
    br.com.omie.core.di.SmartPDVViewModelOwner {

    private var binding: FragmentProductsBinding? = null

    private val viewModel: ProductViewModel by appViewmodel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.init()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return FragmentProductsBinding.inflate(inflater, container, false).apply {
            binding = this

            toolbar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menu_history -> {
                        findNavController().navigate(ProductsFragmentDirections.actionHomeFragmentToHistoryFragment())
                    }
                }
                return@setOnMenuItemClickListener true
            }

        }.root
    }

    private fun configUI(products: List<Product>) {
        binding?.apply {
            val adapter = ProductAdapter(products, this@ProductsFragment)
            rvProducts.adapter = adapter
        }
    }

    private fun configCarts(carts: List<Cart>) {
        binding?.apply {
            fabCart.visibility = if (carts.isNotEmpty()) View.VISIBLE else View.GONE

            fabCart.setOnClickListener {
                viewModel.showCart = !viewModel.showCart
                ctCarts.visibility = if (viewModel.showCart) View.VISIBLE else View.GONE
                val fabIcon = ContextCompat.getDrawable(
                    requireContext(),
                    if (viewModel.showCart) R.drawable.ic_close else R.drawable.ic_cart
                )
                fabCart.setImageDrawable(fabIcon)
            }

            val adapter = CartAdapter(carts, object : CartAdapter.Delegate {
                override fun onCartSelected(cart: Cart) {
                    cart.let {
                        findNavController().navigate(
                            ProductsFragmentDirections.actionHomeFragmentToCartFragment(
                                cart
                            )
                        )
                    }
                }
            })

            rvCarts.adapter = adapter
        }
    }

    private fun observeViewModel() {

        lifecycleScope.launch {
            viewModel.productsFlow.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {

                        },
                        onSuccess = { products ->
                            configUI(products.orEmpty())
                        },
                        onFailure = {

                        },
                    )

                }

        }

        lifecycleScope.launch {
            viewModel.cartsFlow.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = { carts ->
                            configCarts(carts.orEmpty())
                        },
                        onFailure = {

                        },
                    )

                }

        }
    }

    override fun onProductSelected(product: Product) {
        val directions =
            ProductsFragmentDirections.actionHomeFragmentToProductDetailFragment(product)
        findNavController().navigate(directions)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}