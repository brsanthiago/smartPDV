package br.com.omie.smartpdv.ui.splash.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.network.response.data
import br.com.omie.core.data.network.response.isSuccess
import br.com.omie.smartpdv.ui.product.usecase.CountProductUseCase
import br.com.omie.smartpdv.ui.product.usecase.SyncUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class SyncViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val syncUseCase: SyncUseCase,
    private val getCountProductUseCase: CountProductUseCase,
) : ViewModel() {

    private val _syncFlow = MutableStateFlow<AppResult<Unit>>(AppResult.Loading)
    val syncFlow: StateFlow<AppResult<Unit>> = _syncFlow


    fun init() {
        viewModelScope.launch(dispatcher) {
            getCountProductUseCase(CountProductUseCase.Params()).onEach { result ->
                if (result.isSuccess()) {
                    val size = result.data() ?: 0

                    if (size == 0) {
                        getAll()
                    } else {
                        _syncFlow.value = AppResult.Success(null)
                    }
                }
            }.launchIn(this)


        }
    }

    private fun getAll() {
        viewModelScope.launch(dispatcher) {
            syncUseCase(SyncUseCase.Params()).onEach {
                _syncFlow.value = it
            }.launchIn(this)
        }
    }

}