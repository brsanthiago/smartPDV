package br.com.omie.smartpdv.ui.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.omie.core.data.dto.CartItemDTO
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.core.extensions.showConfirmationDialog
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.CartFragmentBinding
import br.com.omie.smartpdv.ui.cart.adapter.CartItemAdapter
import br.com.omie.smartpdv.ui.cart.viewmodel.CartItemViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


internal class CartFragment : Fragment(), SmartPDVViewModelOwner {

    private var binding: CartFragmentBinding? = null
    private val args: CartFragmentArgs by navArgs()
    private val viewModel: CartItemViewModel by appViewmodel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
        args.cart.id?.let { viewModel.getItems(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return CartFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
            configUI()
        }.root
    }

    private fun configUI() {
        binding?.apply {

            btnPayment.setOnClickListener {
                requireActivity().showConfirmationDialog(message = getString(R.string.confirm_payment),
                    primaryText = getString(R.string.payment),
                    title = getString(
                        R.string.lb_payment
                    ),
                    primaryActionButton = {
                        viewModel.confirmPayment(args.cart)
                    }).show()
            }
        }
    }

    private fun loadItems(items: List<CartItemDTO>) {

        binding?.apply {
            val customer = args.cart.customer


            toolbar.title = getString(R.string.customer_cart, customer)

            toolbar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menu_delete -> {

                        requireActivity().showConfirmationDialog(primaryActionButton = {
                            viewModel.deleteCart(args.cart)
                        }).show()
                    }
                }
                return@setOnMenuItemClickListener true
            }

            val adapter = CartItemAdapter(items, object : CartItemAdapter.Delegate {
                override fun onAddItem(item: CartItemDTO) {
                    viewModel.updateItem(item, true)
                }

                override fun onRemoveItem(item: CartItemDTO) {
                    viewModel.updateItem(item, false)
                }

                override fun onDeleteItem(item: CartItemDTO) {

                    requireActivity().showConfirmationDialog(title = getString(R.string.remove_product),
                        message = getString(
                            R.string.remove_product_message
                        ),
                        primaryText = getString(R.string.action_yes),
                        primaryActionButton = {
                            viewModel.deleteItem(item)
                        }).show()
                }

            })

            tvTotal.text = getString(
                R.string.total_amount_cart,
                CommonUtils.formatCurrencyValue(viewModel.totalAmount())
            )

            rvCartItems.adapter = adapter

            btnPayment.isEnabled = items.isNotEmpty()
        }

    }


    private fun observeViewModel() {


        lifecycleScope.launch {

            viewModel.deleteCartFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                            binding?.loader?.root?.visibility = View.VISIBLE
                        },
                        onSuccess = {
                            delay(1500)
                            binding?.loader?.root?.visibility = View.GONE
                            findNavController().popBackStack(R.id.home_fragment, false)

                        },
                        onFailure = { error ->
                            binding?.loader?.root?.visibility = View.GONE
                            println(error?.message)
                        }
                    )
                }
        }

        lifecycleScope.launch {

            viewModel.cartItemsFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                            binding?.loader?.root?.visibility = View.VISIBLE
                        },
                        onSuccess = { data ->
                            binding?.loader?.root?.visibility = View.GONE
                            loadItems(data.orEmpty())

                        },
                        onFailure = {
                            binding?.loader?.root?.visibility = View.GONE
                        }
                    )
                }
        }

        lifecycleScope.launch {

            viewModel.cartFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                            binding?.loader?.root?.visibility = View.VISIBLE
                        },
                        onSuccess = {
                            binding?.loader?.root?.visibility = View.GONE
                            findNavController().popBackStack(R.id.home_fragment, false)

                        },
                        onFailure = {
                            binding?.loader?.root?.visibility = View.GONE
                        }
                    )
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}