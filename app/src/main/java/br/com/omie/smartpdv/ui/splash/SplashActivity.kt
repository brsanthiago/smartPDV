package br.com.omie.smartpdv.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.smartpdv.databinding.ActivitySplashBinding
import br.com.omie.smartpdv.ui.MainActivity
import br.com.omie.smartpdv.ui.splash.viewmodel.SyncViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity(), SmartPDVViewModelOwner {
    private lateinit var binding: ActivitySplashBinding

    private val viewModel: SyncViewModel by appViewmodel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observeViewModel()
        viewModel.init()
    }

    private fun observeViewModel() {

        lifecycleScope.launch {
            viewModel.syncFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = {
                            start()

                        },
                        onFailure = {
                            start()
                        }
                    )
                }
        }
    }

    private fun start() {
        CoroutineScope(Dispatchers.IO).launch(Dispatchers.IO) {
            delay(2000)
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }
    }
}