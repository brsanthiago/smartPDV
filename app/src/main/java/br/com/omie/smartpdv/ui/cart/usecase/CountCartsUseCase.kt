package br.com.omie.smartpdv.ui.cart.usecase

import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartRepository
import br.com.omie.core.data.usecase.UseCase

internal class CountCartsUseCase(private val repository: CartRepository) :
    UseCase<CountCartsUseCase.Params, Int>() {
    class Params

    override suspend fun invoke(params: Params): AppResult<Int> {
        return AppResult.from(
            runCatching {
                repository.getCount()
            }
        )
    }
    /*override suspend fun invoke(params: Params): Flow<AppResult<Int>> = flow {
        emit(AppResult.Loading)

        val result = AppResult.from(
            runCatching {
                repository.getCount()
            }
        )
        emit(result)
    }*/
}