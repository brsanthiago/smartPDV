package br.com.omie.smartpdv.ui.product.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.omie.core.data.model.Product
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.ProductItemBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

internal class ProductAdapter(
    private val products: List<Product>,
    private val delegate: Delegate
) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    interface Delegate {
        fun onProductSelected(product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            ProductItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ).apply {

            binding.root.setOnClickListener {
                delegate.onProductSelected(products[bindingAdapterPosition])
            }
        }
    }

    override fun onBindViewHolder(vh: ProductViewHolder, position: Int) {
        val product = products[position]
        vh.bind(product)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    internal class ProductViewHolder(val binding: ProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: Product) {
            with(binding) {
                tvName.text = product.title
                tvDescription.text = product.description
                tvPrice.text = CommonUtils.formatCurrencyValue(product.price ?: 0.0)

                product.thumbnail?.let { image ->

                    try {
                        Glide.with(ivProduct.context)
                            .load(image)
                            .placeholder(R.drawable.product_placeholder)
                            .error(R.drawable.product_placeholder)
                            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                            .into(ivProduct)
                    } catch (_: Exception) {
                    }
                }


            }
        }
    }
}