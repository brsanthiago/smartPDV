package br.com.omie.smartpdv.ui.cart.usecase

import br.com.omie.core.data.model.CartItem
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartItemRepository
import br.com.omie.core.data.usecase.UseCase

internal class GetCartItemUseCase(private val repository: CartItemRepository) :
    UseCase<GetCartItemUseCase.Params, CartItem?>() {
    class Params(val cartId: Long)

    override suspend fun invoke(params: Params): AppResult<CartItem?> {
        return AppResult.from(
            Result.success(repository.findOne(params.cartId))
        )
    }
}