package br.com.omie.smartpdv.ui.cart.usecase

import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartRepository
import br.com.omie.core.data.usecase.UseCase

internal class DeleteCartUseCase(private val repository: CartRepository) :
    UseCase<DeleteCartUseCase.Params, Int>() {
    class Params(val id: Long)

    override suspend fun invoke(params: Params): AppResult<Int> {
        return AppResult.from(Result.success(repository.delete(id = params.id)))
    }
}