package br.com.omie.smartpdv.ui.history.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.recyclerview.widget.RecyclerView
import br.com.omie.core.data.model.Cart
import br.com.omie.core.extensions.toDateTime
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.HistoryCartItemBinding

internal class HistoryAdapter(
    private val items: List<Cart>,
    private val delegate: Delegate
) : RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {
    interface Delegate {
        fun onItemSelected(cart: Cart)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        return HistoryViewHolder(
            HistoryCartItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ).apply {
            binding.root.setOnClickListener {
                delegate.onItemSelected(items[bindingAdapterPosition])
            }
        }
    }

    override fun onBindViewHolder(vh: HistoryViewHolder, position: Int) {
        val cart = items[position]
        vh.bind(cart)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    internal class HistoryViewHolder(val binding: HistoryCartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context
        fun bind(item: Cart) {
            with(binding) {

                tvCart.text = buildSpannedString {
                    append(context.getString(R.string.request_number, item.id))
                    append("\n")
                    append(
                        context.getString(
                            R.string.request_date,
                            item.paymentDate?.toDateTime() ?: ""
                        )
                    )
                }
                tvCustomer.text = context.getString(R.string.customer, item.customer)
                tvTotal.text = context.getString(
                    R.string.total_amount_cart,
                    CommonUtils.formatCurrencyValue(item.totalAmount ?: 0.0)
                )
                tvQtde.text = context.getString(R.string.total_items, item.totalItems)


            }
        }
    }
}