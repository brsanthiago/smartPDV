package br.com.omie.smartpdv.ui.product.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.model.CartItem
import br.com.omie.core.data.model.Product
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.network.response.data
import br.com.omie.smartpdv.ui.product.usecase.GetProductCartUseCase
import br.com.omie.smartpdv.ui.product.usecase.ProductCartUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class ProductDetailViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val addProductUseCase: ProductCartUseCase,
    private val getProductUseCase: GetProductCartUseCase
) : ViewModel() {

    private val _productsFlow = MutableStateFlow<AppResult<List<Product>>>(AppResult.Loading)
    val productsFlow: StateFlow<AppResult<List<Product>>> = _productsFlow

    private val _cartItemFlow = MutableStateFlow<AppResult<Unit>>(AppResult.Loading)
    val cartItemFlow: StateFlow<AppResult<Unit>> = _cartItemFlow

    fun addToCart(cart: Cart, product: Product) {
        val cartId = cart.id ?: return
        val productId = product.id ?: return

        _productsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {

            val cartItem = getProductUseCase(GetProductCartUseCase.Params(cartId, productId)).data()

            val quantity = cartItem?.quantity ?: 0

            cartItem?.let {
                it.quantity = quantity + 1
            }

            val item = cartItem ?: CartItem(
                cartId = cart.id,
                productId = product.id,
                quantity = 1
            )



            addProductUseCase(ProductCartUseCase.Params(item)).onEach {
                _cartItemFlow.value = it
            }.launchIn(this)
        }
    }
}