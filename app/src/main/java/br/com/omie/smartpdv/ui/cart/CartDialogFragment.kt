package br.com.omie.smartpdv.ui.cart

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.core.extensions.disableDrag
import br.com.omie.core.extensions.expand
import br.com.omie.smartpdv.databinding.CartDialogFragmentBinding
import br.com.omie.smartpdv.ui.cart.adapter.CartAdapter
import br.com.omie.smartpdv.ui.cart.viewmodel.CartViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
internal class CartDialogFragment : BottomSheetDialogFragment(), SmartPDVViewModelOwner {

    var onCartSelected: ((cart: Cart) -> Unit)? = null
    var onCreateCart: (() -> Unit)? = null
    var onDismissView: (() -> Unit)? = null

    private val viewModel: CartViewModel by appViewmodel()
    override fun getTheme(): Int = br.com.omie.core.R.style.BottomSheetDialogTheme

    private var binding: CartDialogFragmentBinding? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewModel.init()
        expand()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                dismiss()
            }
        })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            dialog.behavior.setBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        dialog.dismiss()
                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    if (!slideOffset.isNaN()) dialog.window?.setDimAmount(0.5f - ((slideOffset * -1) / 2))
                }
            })
        }
        disableDrag(dialog)
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return CartDialogFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
            btnClose.setOnClickListener {
                dialog?.dismiss()
                dismiss()
            }

            btnAddCart.setOnClickListener {
                onCreateCart?.invoke()
            }

        }.root
    }

    fun reload() {
        viewModel.init()
    }

    private fun configCarts(carts: List<Cart>) {
        val adapter = CartAdapter(carts, object : CartAdapter.Delegate {
            override fun onCartSelected(cart: Cart) {
                onCartSelected?.invoke(cart)
            }

        })

        binding?.apply {
            if (carts.size >= 5) {
                btnAddCart.visibility = View.GONE
            }
            rvCarts.adapter = adapter
        }

    }

    private fun observeViewModel() {

        lifecycleScope.launch {

            viewModel.cartsFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = { data ->
                            configCarts(data.orEmpty())

                        },
                        onFailure = { error ->
                            println(error?.message)
                        }
                    )
                }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        onDismissView?.invoke()
        binding = null
        super.onDismiss(dialog)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    companion object {
        const val TAG = "CartDialogFragment:Product"
        fun newInstance(): CartDialogFragment {
            return CartDialogFragment()
        }
    }
}