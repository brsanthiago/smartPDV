package br.com.omie.smartpdv.ui.cart.usecase

import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartItemRepository
import br.com.omie.core.data.usecase.UseCase

internal class DeleteItemUseCase(private val repository: CartItemRepository) :
    UseCase<DeleteItemUseCase.Params, Int>() {
    class Params(val id: Long)

    override suspend fun invoke(params: Params): AppResult<Int> {
        return AppResult.from(Result.success(repository.deleteItem(id = params.id)))
    }
}