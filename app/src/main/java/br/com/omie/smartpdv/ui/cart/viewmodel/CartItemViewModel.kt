package br.com.omie.smartpdv.ui.cart.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.dto.CartItemDTO
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.network.response.data
import br.com.omie.smartpdv.ui.cart.usecase.AddCartUseCase
import br.com.omie.smartpdv.ui.cart.usecase.DeleteCartUseCase
import br.com.omie.smartpdv.ui.cart.usecase.DeleteItemUseCase
import br.com.omie.smartpdv.ui.cart.usecase.DeleteItemsUseCase
import br.com.omie.smartpdv.ui.cart.usecase.GetCartItemUseCase
import br.com.omie.smartpdv.ui.cart.usecase.GetCartItemsUseCase
import br.com.omie.smartpdv.ui.product.usecase.GetProductUseCase
import br.com.omie.smartpdv.ui.product.usecase.ProductCartUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.util.Date

internal class CartItemViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val getCartItemsUseCase: GetCartItemsUseCase,
    private val getProductUseCase: GetProductUseCase,
    private val getCartItemUseCase: GetCartItemUseCase,
    private val productCartUseCase: ProductCartUseCase,
    private val deleteItemUseCase: DeleteItemUseCase,
    private val deleteItemsUseCase: DeleteItemsUseCase,
    private val deleteCartUseCase: DeleteCartUseCase,
    private val updateCartUseCase: AddCartUseCase,
) : ViewModel() {

    private val _cartItemsFlow = MutableStateFlow<AppResult<List<CartItemDTO>>>(AppResult.Loading)
    val cartItemsFlow: StateFlow<AppResult<List<CartItemDTO>>> = _cartItemsFlow

    private val _deleteCartFlow = MutableStateFlow<AppResult<Long>>(AppResult.Iddle)
    val deleteCartFlow: StateFlow<AppResult<Long>> = _deleteCartFlow

    private val _cartFlow = MutableStateFlow<AppResult<Unit>>(AppResult.Iddle)
    val cartFlow: StateFlow<AppResult<Unit>> = _cartFlow

    fun totalAmount(): Double {
        var total = 0.0
        _cartItemsFlow.value.data()?.forEach { item ->
            total += (item.product?.price ?: 0.0) * (item.quantity ?: 1)
        }

        return total
    }

    private fun totalItems(): Int {
        var total = 0
        _cartItemsFlow.value.data()?.forEach { item ->
            total += item.quantity ?: 0
        }

        return total
    }

    fun getItems(cartId: Long) {
        _cartItemsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            val cartItems = getCartItemsUseCase(GetCartItemsUseCase.Params(cartId)).data().orEmpty()

            val items = cartItems.map { cartItem ->
                val productId = cartItem.productId!!
                val product = getProductUseCase(GetProductUseCase.Params(productId)).data()
                CartItemDTO(
                    id = cartItem.id,
                    cartId = cartId,
                    product = product,
                    quantity = cartItem.quantity
                )
            }


            _cartItemsFlow.value = AppResult.Success(items)
        }
    }

    fun updateItem(dto: CartItemDTO, incrise: Boolean) {
        val itemID = dto.id ?: return
        val cartId = dto.cartId ?: return
        _cartItemsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {

            val item = getCartItemUseCase(GetCartItemUseCase.Params(itemID)).data() ?: return@launch
            val quantity = if (incrise) item.quantity + 1 else item.quantity - 1
            item.quantity = quantity

            productCartUseCase(ProductCartUseCase.Params(item)).onEach {
                getItems(cartId)
            }.launchIn(this)
        }
    }

    fun deleteItem(dto: CartItemDTO) {
        val itemID = dto.id ?: return
        val cartId = dto.cartId ?: return
        _cartItemsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            deleteItemUseCase(DeleteItemUseCase.Params(itemID)).data()
            getItems(cartId)
        }
    }

    fun deleteCart(cart: Cart) {
        val cartId = cart.id ?: return
        _deleteCartFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            deleteItemsUseCase(DeleteItemsUseCase.Params(cartId))
            val data = deleteCartUseCase(DeleteCartUseCase.Params(cartId)).data() ?: 0
            _deleteCartFlow.value = AppResult.Success(data.toLong())
        }
    }

    fun confirmPayment(cart: Cart) {
        _cartFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            cart.totalAmount = totalAmount()
            cart.totalItems = totalItems()
            cart.paymentDate = Date()
            cart.active = false
            updateCartUseCase(AddCartUseCase.Params(cart)).onEach {
                _cartFlow.value = it
            }.launchIn(this)
        }
    }
}