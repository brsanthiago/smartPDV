package br.com.omie.smartpdv.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import br.com.omie.core.data.dto.CartItemDTO
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.HistoryDetailFragmentBinding
import br.com.omie.smartpdv.ui.cart.viewmodel.CartItemViewModel
import br.com.omie.smartpdv.ui.history.adapter.HistoryItemAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


internal class HisytoryDetailFragment : Fragment(), SmartPDVViewModelOwner {

    private var binding: HistoryDetailFragmentBinding? = null
    private val args: HisytoryDetailFragmentArgs by navArgs()
    private val viewModel: CartItemViewModel by appViewmodel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
        args.cart.id?.let { viewModel.getItems(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return HistoryDetailFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
            configUI()
        }.root
    }

    private fun configUI() {
        binding?.apply {
            tvTitle.text = getString(R.string.customer, args.cart.customer)
        }
    }

    private fun loadItems(items: List<CartItemDTO>) {

        binding?.apply {
            val adapter = HistoryItemAdapter(items)
            tvTotal.text = getString(
                R.string.total_amount_cart,
                CommonUtils.formatCurrencyValue(viewModel.totalAmount())
            )
            rvCartItems.adapter = adapter
        }
    }

    private fun observeViewModel() {

        lifecycleScope.launch {

            viewModel.cartItemsFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = { data ->
                            loadItems(data.orEmpty())

                        },
                        onFailure = {
                        }
                    )
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}