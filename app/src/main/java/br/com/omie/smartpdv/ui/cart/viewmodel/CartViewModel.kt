package br.com.omie.smartpdv.ui.cart.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.smartpdv.ui.cart.usecase.GetCartsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

internal class CartViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val getCartsUseCase: GetCartsUseCase
) : ViewModel() {


    private val _cartsFlow = MutableStateFlow<AppResult<List<Cart>>>(AppResult.Loading)
    val cartsFlow: StateFlow<AppResult<List<Cart>>> = _cartsFlow

    fun init() {
        getAll()
    }

    private fun getAll() {
        _cartsFlow.value = AppResult.Loading
        viewModelScope.launch(dispatcher) {
            viewModelScope.launch(dispatcher) {
                getCartsUseCase(GetCartsUseCase.Params()).onEach { result ->
                    _cartsFlow.value = result
                }.launchIn(this)
            }
        }
    }
}