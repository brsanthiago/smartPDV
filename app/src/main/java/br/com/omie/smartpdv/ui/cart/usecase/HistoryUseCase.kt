package br.com.omie.smartpdv.ui.cart.usecase

import br.com.omie.core.data.model.Cart
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartRepository
import br.com.omie.core.data.usecase.FlowableUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class HistoryUseCase(private val repository: CartRepository) :
    FlowableUseCase<HistoryUseCase.Params, List<Cart>>() {
    class Params

    override suspend fun invoke(params: Params): Flow<AppResult<List<Cart>>> = flow {
        emit(AppResult.Loading)

        val result = AppResult.from(
            runCatching {
                repository.findAll()
            }
        )
        emit(result)
    }
}