package br.com.omie.smartpdv.ui.product.usecase

import br.com.omie.core.data.model.CartItem
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartItemRepository
import br.com.omie.core.data.usecase.UseCase

internal class GetProductCartUseCase(private val repository: CartItemRepository) :
    UseCase<GetProductCartUseCase.Params, CartItem?>() {
    class Params(val cartId: Long, val productId: Long)

    override suspend fun invoke(params: Params): AppResult<CartItem?> {
        return AppResult.from(
            Result.success(repository.findByCart(params.cartId, params.productId))
        )
    }
}