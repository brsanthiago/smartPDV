package br.com.omie.smartpdv.ui.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.ProductDetailFragmentBinding
import br.com.omie.smartpdv.ui.cart.CartDialogFragment
import br.com.omie.smartpdv.ui.cart.NewCartDialogFragment
import br.com.omie.smartpdv.ui.product.adapter.BannerAdapter
import br.com.omie.smartpdv.ui.product.viewmodel.ProductDetailViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

internal class ProductDetailFragment : Fragment(), SmartPDVViewModelOwner {
    private var binding: ProductDetailFragmentBinding? = null
    private val args: ProductDetailFragmentArgs by navArgs()


    private var bottomSheet: CartDialogFragment? = null
    private val viewModel: ProductDetailViewModel by appViewmodel()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ProductDetailFragmentBinding.inflate(inflater, container, false).apply {
            binding = this

            toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }

            val product = args.product
            tvProduct.text = product.title
            tvDescription.text = product.description
            tvPrice.text = CommonUtils.formatCurrencyValue(product.price ?: 0.0)

            btnAddCart.setOnClickListener {
                showAvailableCarts()
            }

            product.images?.let { images ->
                configBanners(images)
            }
        }.root
    }

    private fun configBanners(images: List<String>) {
        binding?.apply {

            val fragments = mutableListOf<Fragment>()
            images.forEach { image ->
                fragments.add(BannerFragment.newInstance(image))
            }

            val viewPagerAdapter = BannerAdapter(requireActivity(), fragments)
            vpBanner.offscreenPageLimit = fragments.size
            vpBanner.adapter = viewPagerAdapter
            vpBanner.isUserInputEnabled = false
            addDotContainers(images)


            btnPrevious.setOnClickListener {
                vpBanner.currentItem = vpBanner.currentItem.minus(1)
                selectDot(vpBanner.currentItem)
            }

            btnNext.setOnClickListener {
                val hasNext = vpBanner.currentItem < fragments.size - 1
                if (hasNext) {
                    vpBanner.currentItem = vpBanner.currentItem.plus(1)
                }

                selectDot(vpBanner.currentItem)
            }

            vpBanner.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    selectDot(position)

                    when {
                        position == 0 -> {
                            btnPrevious.isEnabled = false
                        }

                        position > 0 -> {
                            btnPrevious.isEnabled = true
                        }

                        position == images.size - 1 -> {
                            btnNext.isEnabled = false
                        }

                        position < images.size - 1 -> {
                            btnNext.isEnabled = true
                        }
                    }

                }
            })
        }
    }

    private fun addDotContainers(images: List<String>) {

        for (pos in images.indices) {

            val layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            layoutParams.setMargins(6, 0, 6, 0)
            val dot = ImageView(requireContext())
            dot.id = pos
            dot.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.selected_dot
                )
            )
            dot.adjustViewBounds = true
            dot.layoutParams = layoutParams
            layoutParams.setMargins(8, 0, 8, 0)
            dot.layoutParams = layoutParams

            binding?.flexDots?.addView(dot)
        }
        selectDot(0)
    }

    private fun selectDot(group: Int) {
        binding?.apply {

            clearSelection()

            val container = (flexDots.getChildAt(group) as ImageView)
            val dotId = container.id

            if (dotId == group) {
                container.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.selected_dot
                    )
                )
            } else {
                container.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.default_dot
                    )
                )
            }
        }
    }

    private fun clearSelection() {
        binding?.run {
            for (group in 0 until flexDots.childCount) {
                val container = (flexDots.getChildAt(group) as ImageView)
                container.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.default_dot
                    )
                )
            }
        }
    }

    private fun createCartDialog() {
        if (bottomSheet == null) {
            bottomSheet = CartDialogFragment.newInstance()
            bottomSheet?.show(parentFragmentManager, CartDialogFragment.TAG)
        }
    }

    private fun showAvailableCarts() {
        val product = args.product
        createCartDialog()

        bottomSheet?.isCancelable = false

        bottomSheet?.onCartSelected = { cart ->
            viewModel.addToCart(cart, product)
        }
        bottomSheet?.onCreateCart = {
            val newCartDialog = NewCartDialogFragment()
            newCartDialog.onItemSaved = {
                bottomSheet?.reload()
            }
            newCartDialog.show(parentFragmentManager, product.id.toString())
        }
        bottomSheet?.onDismissView = {
            bottomSheet = null
        }

        lifecycleScope.launch {

            viewModel.cartItemFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = {
                            bottomSheet?.dismissNow()
                            bottomSheet = null
                            binding?.container?.let { it1 ->
                                Snackbar.make(
                                    requireContext(),
                                    it1, getString(R.string.product_added_to_cart), 1500
                                ).show()
                            }
                            findNavController().popBackStack(R.id.home_fragment, false)

                        },
                        onFailure = {
                        }
                    )
                }
        }
    }

    override fun onDestroyView() {
        bottomSheet = null
        binding = null
        super.onDestroyView()
    }
}