package br.com.omie.smartpdv.ui.product.usecase

import br.com.omie.core.data.model.CartItem
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.CartItemRepository
import br.com.omie.core.data.usecase.FlowableUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class ProductCartUseCase(private val repository: CartItemRepository) :
    FlowableUseCase<ProductCartUseCase.Params, Unit>() {
    class Params(val item: CartItem)

    override suspend fun invoke(params: Params): Flow<AppResult<Unit>> = flow {
        emit(AppResult.Loading)

        val result = AppResult.from(
            runCatching {
                repository.saveOne(params.item)
            }
        )
        emit(result)
    }
}