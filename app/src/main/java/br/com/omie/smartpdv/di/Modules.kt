package br.com.omie.smartpdv.di

import android.content.Context
import br.com.omie.core.data.database.SmartDatabase
import br.com.omie.core.data.network.AppCoroutinesServiceBuilder
import br.com.omie.core.data.network.api.ProductAPI
import br.com.omie.core.data.repository.CartItemRepository
import br.com.omie.core.data.repository.CartRepository
import br.com.omie.core.data.repository.ProductRepository
import br.com.omie.core.data.repository.RemoteProductRepository
import br.com.omie.core.di.SmartPDVKoinContext
import br.com.omie.smartpdv.ui.cart.usecase.AddCartUseCase
import br.com.omie.smartpdv.ui.cart.usecase.CountCartsUseCase
import br.com.omie.smartpdv.ui.cart.usecase.DeleteCartUseCase
import br.com.omie.smartpdv.ui.cart.usecase.DeleteItemUseCase
import br.com.omie.smartpdv.ui.cart.usecase.DeleteItemsUseCase
import br.com.omie.smartpdv.ui.cart.usecase.GetCartItemUseCase
import br.com.omie.smartpdv.ui.cart.usecase.GetCartItemsUseCase
import br.com.omie.smartpdv.ui.cart.usecase.GetCartsUseCase
import br.com.omie.smartpdv.ui.cart.usecase.HistoryUseCase
import br.com.omie.smartpdv.ui.cart.viewmodel.CartItemViewModel
import br.com.omie.smartpdv.ui.cart.viewmodel.CartViewModel
import br.com.omie.smartpdv.ui.cart.viewmodel.NewCartViewModel
import br.com.omie.smartpdv.ui.history.viewmodel.HistoryViewModel
import br.com.omie.smartpdv.ui.product.usecase.CountProductUseCase
import br.com.omie.smartpdv.ui.product.usecase.GetProductCartUseCase
import br.com.omie.smartpdv.ui.product.usecase.GetProductUseCase
import br.com.omie.smartpdv.ui.product.usecase.ProductCartUseCase
import br.com.omie.smartpdv.ui.product.usecase.ProductsUseCase
import br.com.omie.smartpdv.ui.product.usecase.SyncUseCase
import br.com.omie.smartpdv.ui.product.viewmodel.ProductDetailViewModel
import br.com.omie.smartpdv.ui.product.viewmodel.ProductViewModel
import br.com.omie.smartpdv.ui.splash.viewmodel.SyncViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

internal val appModule = module {

    single { SmartDatabase.invoke(androidApplication()) }
    factory { AppCoroutinesServiceBuilder().build(ProductAPI::class.java) }
    single { RemoteProductRepository(get()) }
    single { SyncUseCase(get(), get()) }

    single { ProductRepository(get()) }
    single { CartRepository(get()) }
    single { CartItemRepository(get()) }

    single { DeleteCartUseCase(get()) }
    single { DeleteItemsUseCase(get()) }
    single { GetCartItemsUseCase(get()) }
    single { GetProductUseCase(get()) }
    single { GetCartItemUseCase(get()) }
    single { DeleteItemUseCase(get()) }
    single { ProductsUseCase(get()) }
    single { CountProductUseCase(get()) }
    single { ProductCartUseCase(get()) }
    single { GetProductCartUseCase(get()) }
    single { GetCartsUseCase(get()) }
    single { AddCartUseCase(get()) }
    single { CountCartsUseCase(get()) }
    single { HistoryUseCase(get()) }

    viewModel {
        HistoryViewModel(
            dispatcher = Dispatchers.IO,
            getHistoryUseCase = get()
        )
    }

    viewModel {
        CartViewModel(
            dispatcher = Dispatchers.IO,
            getCartsUseCase = get()
        )
    }

    viewModel {
        ProductViewModel(
            dispatcher = Dispatchers.IO,
            getProductsUseCase = get(),
            getCartsUseCase = get(),
        )
    }

    viewModel {
        ProductDetailViewModel(
            dispatcher = Dispatchers.IO,
            addProductUseCase = get(),
            getProductUseCase = get()
        )
    }

    viewModel {
        NewCartViewModel(
            dispatcher = Dispatchers.IO,
            addCartUseCase = get(),
            countCartsUseCase = get()
        )
    }

    viewModel {
        CartItemViewModel(
            dispatcher = Dispatchers.IO,
            getCartItemsUseCase = get(),
            getProductUseCase = get(),
            getCartItemUseCase = get(),
            productCartUseCase = get(),
            deleteItemUseCase = get(),
            deleteItemsUseCase = get(),
            deleteCartUseCase = get(),
            updateCartUseCase = get()
        )
    }

    viewModel {
        SyncViewModel(
            dispatcher = Dispatchers.IO,
            syncUseCase = get(),
            getCountProductUseCase = get(),
        )
    }

}


@Suppress("unused")
internal class SmartPDVKoinInjector : br.com.omie.core.di.ModuleConfigInjector {
    override fun inject(context: Context) {
        SmartPDVKoinContext.loadModules(appModule)
    }

}