package br.com.omie.smartpdv.ui.cart

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import br.com.omie.core.data.network.response.fold
import br.com.omie.core.di.SmartPDVViewModelOwner
import br.com.omie.core.di.appViewmodel
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.NewCartDialogFragmentBinding
import br.com.omie.smartpdv.ui.cart.viewmodel.NewCartViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

internal class NewCartDialogFragment : DialogFragment(), SmartPDVViewModelOwner {
    private val viewModel: NewCartViewModel by appViewmodel()

    var onItemSaved: (() -> Unit)? = null

    private var binding: NewCartDialogFragmentBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                dismiss()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return NewCartDialogFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
            btnClose.setOnClickListener {
                dismiss()
            }

            btnContinue.setOnClickListener {
                val customer = edtText.text.toString().uppercase()

                tiName.error = null

                if (customer.isEmpty()) {
                    tiName.error = "Informe o nome do cliente!"
                    return@setOnClickListener
                }

                viewModel.addNewCart(customer)

            }

        }.root
    }

    private fun observeViewModel() {
        lifecycleScope.launch {

            viewModel.cartFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collectLatest { result ->
                    result.fold(
                        onLoading = {
                        },
                        onSuccess = {
                            onItemSaved?.invoke()
                            dismiss()

                        },
                        onFailure = { error ->
                            val totalCarts = viewModel.totalCarts

                            if (totalCarts >= 5) {
                                binding?.tiName?.error = getString(R.string.error_on_save_cart)
                            } else {
                                binding?.tiName?.error = error?.message
                            }

                            binding?.btnClose?.isEnabled = true
                            binding?.btnContinue?.isEnabled = true
                        }
                    )
                }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        dialog.cancel()
        super.onDismiss(dialog)
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }
}