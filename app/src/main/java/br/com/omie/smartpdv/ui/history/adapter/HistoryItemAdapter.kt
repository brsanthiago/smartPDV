package br.com.omie.smartpdv.ui.history.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.recyclerview.widget.RecyclerView
import br.com.omie.core.data.dto.CartItemDTO
import br.com.omie.core.extensions.toDateTime
import br.com.omie.core.util.CommonUtils
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.HistoryItemBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

internal class HistoryItemAdapter(
    private val items: List<CartItemDTO>
) : RecyclerView.Adapter<HistoryItemAdapter.HistoryItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryItemViewHolder {
        return HistoryItemViewHolder(
            HistoryItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(vh: HistoryItemViewHolder, position: Int) {
        val cart = items[position]
        vh.bind(cart)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    internal class HistoryItemViewHolder(val binding: HistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context
        fun bind(item: CartItemDTO) {
            with(binding) {

                tvProduct.text = item.product?.title
                tvDescription.text = item.product?.description
                tvPrice.text = buildSpannedString {
                    append(item.quantity.toString())
                    append("x ")
                    append(
                        CommonUtils.formatCurrencyValue(item.product?.price ?: 0.0)
                    )
                }

                item.product?.thumbnail?.let { image ->
                    try {
                        Glide.with(context)
                            .load(image)
                            .placeholder(R.drawable.product_placeholder)
                            .error(R.drawable.product_placeholder)
                            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                            .into(ivProduct)
                    } catch (_: Exception) {
                    }
                }

            }
        }
    }
}