package br.com.omie.smartpdv.ui.product

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.BannerFragmentBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

internal class BannerFragment : Fragment() {

    private var binding: BannerFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return BannerFragmentBinding.inflate(inflater).apply {
            binding = this
            configUI()
        }.root
    }

    private fun configUI() {
        binding?.apply {
            val image = arguments?.getString(EXTRA_BANNER) ?: return

            try {
                Glide.with(requireContext())
                    .load(image)
                    .placeholder(R.drawable.product_placeholder)
                    .error(R.drawable.product_placeholder)
                    .apply(
                        RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).override(250, 250)
                    ).listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                    })
                    .into(ivBanner)
            } catch (_: Exception) {
            }

        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    companion object {

        private const val EXTRA_BANNER = "image"
        fun newInstance(image: String) = BannerFragment().apply {
            arguments = bundleOf(EXTRA_BANNER to image)
        }
    }
}