package br.com.omie.smartpdv.ui.product.usecase

import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.ProductRepository
import br.com.omie.core.data.repository.RemoteProductRepository
import br.com.omie.core.data.usecase.FlowableUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class SyncUseCase(
    private val repository: RemoteProductRepository,
    private val localRepository: ProductRepository
) : FlowableUseCase<SyncUseCase.Params, Unit>() {
    class Params

    override suspend fun invoke(params: Params): Flow<AppResult<Unit>> = flow {
        emit(AppResult.Loading)

        val result = AppResult.from(
            runCatching {
                val products = repository.getProducts().data
                localRepository.saveAll(products)
            }
        )
        emit(result)
    }
}