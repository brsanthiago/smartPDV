package br.com.omie.smartpdv.ui.cart.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.omie.core.data.model.Cart
import br.com.omie.smartpdv.R
import br.com.omie.smartpdv.databinding.CustomerCartItemBinding

internal class CartAdapter(
    private val carts: List<Cart>,
    private val delegate: Delegate
) : RecyclerView.Adapter<CartAdapter.CartViewHolder>() {

    interface Delegate {
        fun onCartSelected(cart: Cart)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        return CartViewHolder(
            CustomerCartItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ).apply {

            binding.btnAdd.setOnClickListener {
                delegate.onCartSelected(carts[bindingAdapterPosition])
            }
        }
    }

    override fun onBindViewHolder(vh: CartViewHolder, position: Int) {
        val cart = carts[position]
        vh.bind(cart)
    }

    override fun getItemCount(): Int {
        return carts.size
    }

    internal class CartViewHolder(val binding: CustomerCartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context
        fun bind(item: Cart) {
            with(binding) {

                btnAdd.text = context.getString(R.string.customer_cart, item.customer)

            }
        }
    }
}