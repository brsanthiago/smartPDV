package br.com.omie.smartpdv.ui.product.usecase

import br.com.omie.core.data.model.Product
import br.com.omie.core.data.network.response.AppResult
import br.com.omie.core.data.repository.ProductRepository
import br.com.omie.core.data.usecase.UseCase

internal class GetProductUseCase(private val repository: ProductRepository) :
    UseCase<GetProductUseCase.Params, Product?>() {
    class Params(val id: Long)

    override suspend fun invoke(params: Params): AppResult<Product?> {
        return AppResult.from(
            Result.success(repository.findOne(params.id))
        )
    }
}